<?php

function createCells(array $cells, array $people)
{
    $mmmm = array_filter([null, 4,25]);
    $wwww = array_filter([null, 4, 8]);
    shuffle($cells);
    $mens = $cells;

    shuffle($cells);
    $womens = $cells;

    foreach ($mmmm as $k=>$v) {
        array_splice($mens, $v,0, $k);
    }
    foreach ($wwww as $k=>$v) {
        array_splice($womens, $v,0, $k);
    }

    foreach ($people as $person) {
        list($name, $color, $m, $w) = $person;
        foreach ($m as $m_index) {
            $mens[$m_index] = [
                'name'  => $name,
                'color' => $color,
                'entry' => $mens[$m_index],
            ];
        }
        foreach ($w as $w_index) {
            $womens[$w_index] = [
                'name'  => $name,
                'color' => $color,
                'entry' => $womens[$w_index],
            ];
        }
    }

    return [
        $mens, $womens,
    ];
}

$people = [
    ['Andy', 'red', [0, 8, 18, 28], [1, 16, 17, 19]],
    ['Kaci', 'orange', [1, 16, 20, 26], [5, 11, 18, 21]],
    ['Bree', 'blue', [10, 13, 15, 21,], [3, 12, 23, 28,]],
    ['Dave', 'green', [3, 14, 22, 24], [0, 15, 24, 29]],
    ['Melissa', 'purple', [5, 6, 23, 27,], [6, 14, 22, 27,]],
    ['MO', 'pink', [2, 11, 12, 17,], [7, 9, 25, 26]],
    ['Alex', 'gray', [7, 9, 19, 29], [2, 10, 13, 20,]],
];

list($men_cells, $women_cells) = createCells(range(3, 30), $people);

$men = [
    'id'    => '2020-mens-rumble',
    'title' => '2020 Mens\' Rumble',
    'cells' => $men_cells,
];
$women = [
    'id'    => '2020-womens-rumble',
    'title' => '2020 Womens\' Rumble',
    'cells' => $women_cells,
];

return [
    'production'  => false,
    'baseUrl'     => '',
    'collections' => [
        'rumbles' => [
            'extends' => '_layouts.rumble',
            'path'    => 'rumbles/{id}',
            'items'   => [$men, $women],
        ],
    ],
];
