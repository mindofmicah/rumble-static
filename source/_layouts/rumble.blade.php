@extends('_layouts.master')

@section('body')

    <style>
        .grid > *{
            width:100%;
        }
        .grid > *::before{
            content:"";
            display: inline-block;
            width: 1px;
            height: 0;
            padding-bottom:100%;
        }
        .content{
            position: absolute;;
            top:0;
            left:0;
            bottom:0;
            width:100%;
            display:grid;;
            justify-items: start;
            align-items: end;
        }
    </style>

    <h1 class="text-2xl text-center">{{$page->title}}</h1>

    <div class="grid" style="display:grid; place-items: start; grid-template-columns: repeat(6,1fr)">
        @foreach($page->cells as $cell)
            <div class="{{is_array($cell)?"bg-{$cell['color']}-400":'bg-black text-white'}}" style="position: relative;width: 100%; border:1px solid black;">
                <div class="content" style="align-items: center; justify-items: center; text-align: center;">
                    <div class="content-inside" style="position:absolute">
                        @if(is_array($cell))
                            <div>{{$cell['name']}}</div>
                            <div>{{$cell['entry']}}</div>
                        @else
                            {{$cell}}
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="text-center">
        <a href="/" class="hover:underline">Back to Home</a>
    </div>
@endsection