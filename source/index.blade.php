@extends('_layouts.master')

@section('body')
    <h1>2020 Rumbles</h1>
    <?php
    ?>

    <ul>
        @foreach($rumbles as $rumble)
            <li><a href="{{$rumble->_meta->path[0]}}">{{$rumble->title}}</a></li>
        @endforeach
    </ul>
@endsection
